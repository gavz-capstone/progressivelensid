# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 19:07:22 2020

@author: Geoff
"""

def lenInformationExtraction(df, codes, OCRDescription):

    is_temporal = None
    #determine number of lines of text
    num_lines=len(df)
    
    #blank lens
    if (num_lines==0 or (num_lines==1 and df['description'][0]=='')):  
            is_temporal = True
            return is_temporal
    
    line_i=0
    line1 = df['description'][line_i]
    
    #remove potential junk from first lines - no first line should be less than 3 char or larger than 5
    skipped_lines = 0
    while ((len(line1)<3 or len(line1)>5) and line_i<num_lines):
        #skip first line
        line_i=line_i+1
        skipped_lines+=1
        
        if (skipped_lines!=num_lines): 
            line1 = df['description'][line_i]
        else: 
            #blank lens (all line where skipped - just junk)
            is_temporal = True
            return is_temporal
    
    #update OCR description to display detected text to the user
    for i in range(skipped_lines, num_lines):
        if (df['description'][i] != ''):
            OCRDescription.append(df['description'][i])
        
    #A) Determine if image displays temporal or nasal codes
    # If line 1 has exactly 3 numerical digits then ADD
    if (len(line1)==3 and line1.isdigit()):
        codes['ADD']=line1
        is_temporal=True
    else:
        is_temporal=False
          
    #B) If temporal read any other lines for data
    if(is_temporal==True and num_lines>(1+skipped_lines)):
        #check extra line
        line_i=line_i + 1
        line=df['description'][line_i]
        
        #Frame fit (if 2 digits number)
        if (len(line)==2 and line.isdigit()):
             codes['frame_fit']=line
        # Frame Fit & Base Curve (if 3 digits)
        if (len(line)==3):
             codes['frame_fit']=line[0:2]
             codes['base_curve']=line[2]
        # Frame Fit, Vision Profile, & Base Curve (if 4 characters)
        if (len(line)==4):
             codes['frame_fit']=line[0:2]
             codes['vision_profile']=line[2]
             
             #AUTOCORRECT vision profile
             #Vision Profile code should have no numerical values
             if (codes['vision_profile'].isalpha()==False ):
                 codes['vision_profile']=autocorrectToLetters(line[2])
                 #Update Change in OCR Description String
                 lineToFix = OCRDescription[line_i-skipped_lines]
                 OCRDescription[line_i-skipped_lines]=lineToFix[0:2] + codes['vision_profile'] + lineToFix[3:]
             
             codes['base_curve']=line[3]
        # Frame Fit & MID (if 5 digit)
        if (len(line)==5 and line.isdigit()):
            codes['frame_fit']=line[0:2] 
            codes['MID']=line[2:5]

    #C) nasal codes
    if(is_temporal==False):
        #i) Read product type and material
        #Product type first 2 characters of line 1 if there are 4 characters 
        if (len(line1)==4):
            codes['product']=line1[0:2]
            codes['material']=line1[2:4]
        #Product type first 3 characters if there are 3 or more characters.
        elif(len(line1)>4):
            codes['product']=line1[0:3]
            codes['material']=line1[3:5]
        
        #AUTOCORRECT Product Code
        #product code should have no numerical values (other than 2,3,8)
        if (codes['product'].isalpha()==False):
            codes['product']=autocorrectToLetters(codes['product'])
            #Update Change in OCR Description String
            lineToFix = OCRDescription[line_i-skipped_lines]
            productCodePos=len(codes['product'])
            OCRDescription[line_i-skipped_lines]= codes['product'] + lineToFix[productCodePos:]
        
        #ii) Read Fit height
        if(num_lines>1+skipped_lines):
            line_i=line_i+1
            line2=df['description'][line_i]
            if (len(line2)==2 and line2.isdigit()):
                codes['fit_height']=line2

    return is_temporal
            

####################################################################
#Autocorrect Script
#Used for Prodcut Codes and Vision Profile Codes
####################################################################
def autocorrectToLetters(code):
    codeCorrected = code
    #2,3,8 are valid for product codes
    
    #replace 0 with D
    if (code.find('0') != -1):
        errorIndex = code.find('0')
        codeCorrected = code[0:errorIndex] + 'D' + code[errorIndex+1:]
    
   #replace 1 with I
    if (code.find('1') != -1):
        errorIndex = code.find('1')
        codeCorrected = code[0:errorIndex] + 'I' + code[errorIndex+1:]
        
    #replace 6 with b
    if (code.find('6') != -1):
        errorIndex = code.find('6')
        codeCorrected = code[0:errorIndex] + 'b' + code[errorIndex+1:]

    return codeCorrected
