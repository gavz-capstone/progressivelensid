# +
# @author: Geoff, Vicky
# This script uses Google's text recognition API to detect the text of a preprocessed image
# -

# +
# Library imports
# -
import os, io
import cv2 as cv
from google.cloud import vision
import pandas as pd
from matplotlib import pyplot as plt

import preprocessImages # Import our preprocessing module, preprocessImages.py

confidence_thresh=0.5

# Set the environment variable of GOOGLE_APPLICATION_CREDENTIALS to the path to our JSON token file. 
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.abspath('ServiceAccountToken.json')

# Initialize the client environment for text recognition
client = vision.ImageAnnotatorClient()

# + 
# readTextFromImage(): Takes an input image file name, applies image preprocessing, and calls Google's text
# recognition API. The part of the response relating to the text detected is returned to the user in a DataFrame 
# object.
#
# Parameters: 
#   inputImageName  - [string] The filepath and/or name of the image to read text from (ex. img1.jpg, or images/img1.png)
#
# Returns:
#   df              - [DataFrame object] Text recognition part of the response returned from Google's text recongition API
#
# -

def readTextFromImage(inputImageName):
        # The path to the image (or just the name) relative to the current working directory i.e. the folder this script is in)
    input_image_path = f'{inputImageName}'

    # Call the preprocessing image module to apply the processing filter to inputImageName and save the new,

    # processed image as a numpy array in "output_image"
    output_image = preprocessImages.preprocessImage(input_image_path) # Returns the path to a .jpg 
    
    #Visualize pre-processed image for testing
    # plt.imshow(output_image,'gray')
    # plt.show()
    
    # Convert the output image to a byte stream, and save it to "content"
    success, encoded_image = cv.imencode('.png', output_image)
    content = encoded_image.tobytes()
    
    image = vision.Image(content=content)

    response = client.document_text_detection(image=image)
    
    # Annotate Image Response, and save to DataFrame, "df"
    df = pd.DataFrame(columns=['description', 'confidence'])
    #keep track of verical positions of words
    y_coord = []
    #keep track of df and y_pos index
    y_index = 0
    df_index = 0
    for page in response.full_text_annotation.pages:
        for block in page.blocks:
            #print('\nBlock confidence: {}\n'.format(block.confidence))

            for paragraph in block.paragraphs:
                # print('Paragraph confidence: {}'.format(
                #     paragraph.confidence))

                for word in paragraph.words:
                    word_text = ''.join([
                        symbol.text for symbol in word.symbols if symbol.text.isdigit() or symbol.text.isalpha()  #filter out any symbolic characters
                    ])
                    # print('Word text: {} (confidence: {})'.format(
                    #     word_text, word.confidence))
                          
                    # Save word from each line to data frame
                    # Only save those with a high confidence
                    if (word.confidence>=confidence_thresh):
                        #keep track of verical positions (y_pos)
                        y_coord.append(word.bounding_box.vertices[0].y)
                                               
                        #Special case fix:
                        #if a word has the very close y_pos as the previous word but for some reason was split up, saved to the same line
                        if (y_index>0 and (y_coord[y_index-1]-10 <= y_coord[y_index] <= y_coord[y_index-1]+10)): #check if last y_pos is +/-10 current y_pos
                            df.loc[df_index-1, ('description')]+= word_text
                            
                        else:
                           df = df.append(
                                dict(
                                    description=word_text,
                                    confidence=word.confidence
                                ), ignore_index=True
                            )
                           #new element if df -> incr index
                           df_index = df_index + 1
                           
                        #new elemeent in y_pos -> incr index
                        y_index = y_index + 1
                        
                    # for symbol in word.symbols:
                    #       print('\tSymbol: {} (confidence: {})'.format(
                    #           symbol.text, symbol.confidence))  

    # Return the constructed DataFrame object
    return df

def detect_logos(path):
    """Detects logos in the file."""
    from google.cloud import vision
    import io
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.Image(content=content)

    response = client.logo_detection(image=image)
    logos = response.logo_annotations
    print('Logos:')

    for logo in logos:
        print(logo.description)

    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))