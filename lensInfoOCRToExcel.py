# +
# @author: Vicky
# This script extracts from a lens image the detected alphanumeric characters, and confidence levels,
# and appends to an excel file with the file name and other data. This data is later used to improve
# precision through the collected insights (i.e. autocorrect and precision scores)
#
# Dependencies:
#   xlwt library for reading excel sheets: pip install xlwt
#
# -

# +
# Library imports
# -
import glob
import xlwt

# +
# Script imports
# -
import OCR

# +
# processOCRData(): This method applies OCR to all .jpg and .png files in a specified folder, and manipulates the response 
# for writing into an Excel file (later done in writeOCRDataExcel())
#
# Parameters:
#   folderName - [string] The name of the folder with the images to perform OCR on 
# 
# Returns:
#   OCRData    - [2D Array] The formatted OCR Data, where each data[n] represents a row (data[n] is itself an array []), 
#                and data[n][n] represents data for a cell (ex. "image.png"), following the order of columnNamesOCRExcel
# - 
def processOCRData(folderName):
    # Create an empty array that will later store OCR data 
    OCRData = []

    # The first row of the excel sheet specifies what each column represents:
    #   filename       - The name of the image file that OCR is performed on
    #   lineNumber     - The "line" of text (i.e. 0, 1, ...) that was detected
    #   descriptionOCR - The alphanumeric characters in the current lineNumber, detected by OCR
    #   confidence     - The confidence level (a float between 0 and 1) indicating how accurate "description"" is likely to be
    columnNamesOCRExcel = ["filename", "lineNumber", "descriptionOCR", "confidence"]
    OCRData.append(columnNamesOCRExcel)

    # Get all .jpg and .png images in the folder called "folderName"
    filesInFolder = glob.glob(folderName + '/*.jpg')  + glob.glob(folderName + '/*.png')

    # Iterate over every .png and .jpg file in the folder, perform OCR, and manipulate the response in an ordered array that 
    # gets appended to OCRData
    for filename in filesInFolder:
        # Read the text in an image and store the response in df
        df = OCR.readTextFromImage(filename)

        # If the dataframe returned from OCR has data in it (not None or empty), extract the detected text and confidence 
        if(df is not None and not df.empty):
            # For every detected "line" of text in the image, get the "description" and confidence" values and append them to OCRData array
            for dfIndex in range (0,len(df)):
                description = df['description'][dfIndex]
                confidence  = df['confidence'][dfIndex]
                
                # Create a new array in OCRData - this array represents a row in the Excel sheet, and must follow
                # the order of columnNamesOCRExcel. See above decleration of columnNamesOCRExcel for correct order:
                OCRData.append([filename, dfIndex, description, confidence])

    return OCRData
        

# +
# writeOCRDataExcel(): This function takes the formatted OCR Data array and writes it out to an Excel file. 
#
# Parameters:
#   dataToWrite - [2D Array] The formatted OCR Data, where each data[n] represents a row (data[n] is itself an array []), 
#                 and data[n][n] represents data for a cell (ex. "image.png")
# 
# Returns:
#   N/A
# - 
def writeOCRDataExcel(dataToWrite):
    # Create a new Excel workbook to be saved as {workbookName}.xls, with a sheet called "OCRData" where all data is written
    workbookName = "lensInfoOCRDatanew.xls"
    workbook     = xlwt.Workbook()
    sheet        = workbook.add_sheet('OCRData', cell_overwrite_ok=True)

    # Write all data into the excel sheet, where all row data is specified by dataToWrite[i] and each cell value is at 
    # dataToWrite[i][j] (i = row number, j = column number)
    lastfileName = 0
    i=0
    for rowNumber, rowDataArray in enumerate(dataToWrite):
        #Create new line for every image
        fileName = rowDataArray[0]
        if (fileName != lastfileName) and lastfileName!=0:
            i=i+1
        lastfileName = fileName
        
        for columnNumber, cellData in enumerate(rowDataArray):
            sheet.write(rowNumber+i, columnNumber, cellData)
       
    # Save the workbook with the data written into it
    workbook.save(workbookName)

    
if __name__ == "__main__":
    # First, perform OCR on all images in the "images" folder -> processOCRData(), then get the formatted
    # output data and write it to an Excel sheet -> writeOCRDataExcel()
    OCRData = processOCRData("images")
    writeOCRDataExcel(OCRData)