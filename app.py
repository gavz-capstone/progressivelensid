# +
# Library imports
# -
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import dash_table
import plotly.graph_objects as go

import base64
import os
import sys


# Script imports
import OCR
import yolo_object_detection as OSR
import decoder 


# Initialize original labels with None type
lensPropertiesDict = {
            "Manufacturer" : "N/A", 
            "Product Line" : "N/A",
            "Material" : "N/A",
            "ADD" : "N/A",
            "Frame Fit" : "N/A",
            "Base Curve" : "N/A",
            "Vision Profile" : "N/A",
            "MID" : "N/A",
            "Fit Height" : "N/A",
            "Glasses Side" : "N/A"
}

lensDectectedTextDict = {
    "Left Symbol" : "Enter user edits", 
    "Left Text" : "Enter user edits",
    "Right Symbol" : "Enter user edits",
    "Right Text" : "Enter user edits"
}

userText = pd.DataFrame(lensDectectedTextDict.items(), columns=['Description', 'Detected Text'])
 
# Get the path to the upload directory to temporarily store lens images - this is in the current directory, under the "upload" folder
UPLOAD_DIRECTORY = os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), "upload")

# If the "upload" folder does not yet exist, make one. 
if not os.path.exists(UPLOAD_DIRECTORY):
    os.makedirs(UPLOAD_DIRECTORY)

# TODO: Replace this with actual values from main.py once the script calculates them - most likely through lensPropertiesDict
# fig = go.Figure(go.Bar(
#     x=[60, 75, 78, 76, 87],
#     y=['Log Loss %','F1 Score %','Recall %','Precision %','Accuracy %'],
#     orientation='h',
# ))
# df2 = pd.read_csv('modelscores.csv')

app = dash.Dash(__name__)

app.layout = html.Div([

    html.Div(
        className="app-header",
        children=[
            html.Div('PROGRESSIVE LENS IDENTIFIER', className="app-header--title")
        ]
    ), #HEADER COMPONENT

    html.Div(
        className="app-body",
        children=[
            html.Div([
                html.H6('Left Side of Lens Image Upload:'),
                dcc.Upload(
                    id='lens-images-left',
                    children=html.Div([
                        'Drag and Drop or ',
                        html.A('Select Files')
                    ]),
                    style={
                        'width': '50%',
                        'height': '60px',
                        'lineHeight': '60px',
                        'borderWidth': '1px',
                        'borderStyle': 'dashed',
                        'borderRadius': '5px',
                        'textAlign': 'center',
                        'marginLeft': '25%',
                        'marginBottom': '3%',
                        # 'marginTop': '-2%'
                    },
                    # Allows multiple files to be uploaded
                    multiple=True
                ),
                html.H6('Right Side of Lens Image Upload:'),
                dcc.Upload(
                    id='lens-images-right',
                    children=html.Div([
                        'Drag and Drop or ',
                        html.A('Select Files')
                    ]),
                    style={
                        'width': '50%',
                        'height': '60px',
                        'lineHeight': '60px',
                        'borderWidth': '1px',
                        'borderStyle': 'dashed',
                        'borderRadius': '5px',
                        'textAlign': 'center',
                        'marginLeft': '25%',
                        'marginBottom': '3%',
                        # 'marginTop': '-2%'
                    },
                    # Allows multiple files to be uploaded
                    multiple=True
                ),
                html.Div(id='image-upload-left'),
                html.Div(id='image-upload-right'),
                html.Div(style={
                    'clear':'both'
                }),

                # html.Div(id='manufacturer'),

                html.H6('Detected Text:', style={'width': '50%', 'textAlign':'center'}),

                html.H6('Manual Modifications: (Seperate new lines with comma)', 
                    style={
                        'width': '50%',
                        'float':'right',
                        'textAlign':'center',
                        'margin-top': '-62px',
                        }),


            html.Div([
                    dash_table.DataTable(
                    id='editTable', 
                        data = userText.to_dict('records'),
                        columns=[{"name": i, "id": i} for i in ['Description', 'Detected Text']], # TODO: find how to make this from lensproperties df
                        style_cell={
                                    'textAlign': 'right', 
                                    'backgroundColor': '#D9E5FC', 
                                    'border': 'none',
                                    'fontWeight': 'bold',
                                    'width' :'100%',
                                    'fontSize':'18px'
                        },
                        # style_data_conditional=[{
                        #     'if' : {
                        #         'row_index': 'even',
                        #     },
                        #     'display':'none'
                        # }],
                        style_cell_conditional=[{
                            'if' : {
                            'column_id':'Detected Text'},
                            'textAlign':'left',
                            'padding-left' : '30px',
                            'fontWeight':'normal'
                        }],
                        style_header={
                            'display': 'none'
                        },
                    )], style={
                        'width':'50%',
                        'display': 'inline-flex',
                        'justify-content': 'center'
                        
                        }), #end of lens info table
                
               
                html.Div(
                    id='div2',
                    children=
                    [dash_table.DataTable(
                        id='table-editing-input',
                        data = userText.to_dict('records'),
                        columns=[{"name": i, "id": i} for i in ['Description', 'Detected Text']], # TODO: find how to make this from lensproperties df
                        style_cell={'textAlign': 'center', 
                                    'backgroundColor': '#D9E5FC', 
                                    'border': 'none',
                                    'fontWeight': 'bold',
                                    'width' :'100%',
                                    'fontSize':'18px'
                        },
                        style_cell_conditional=[{
                            'if' : {
                            'column_id':'Detected Text'},
                            'textAlign':'left',
                            'padding-left' : '30px',
                            'fontWeight':'normal'
                        }],
                        # style_data_conditional=[{
                        #     'if' : {
                        #         'column_id': 'Description',
                        #     },
                        #     'display':'none'
                        # }],
                        style_header={
                            'display': 'none'
                        },
                        editable=True
                    )], 
                    style={
                        'width': '50%',
                        'display': 'inline-flex',
                        'margin-top':'12px',
                         'justify-content': 'center'
                        
                        }), #end of lens info table

                
                html.H6('Lens Information:', style={'textAlign':'center'}),

                html.Div(
                    dash_table.DataTable(
                        id='table',
                        columns=[{"name": i, "id": i} for i in ['Title', 'Info']], # TODO: find how to make this from lensproperties df
                        style_cell={'textAlign': 'right', 
                                    'backgroundColor': '#D9E5FC', 
                                    'border': 'none',
                                    'fontWeight': 'bold',
                                    'width' :'50%',
                                    'fontSize':'18px'
                        },
                        style_cell_conditional=[{
                            'if' : {
                                'column_id':'Info'},
                            'textAlign':'left',
                            'padding-left' : '30px',
                            'fontWeight':'normal'
                        }],
                        style_header={
                            'display': 'none'
                        },
                        data = pd.DataFrame(lensPropertiesDict.items(), columns=['Title', 'Info']).to_dict('records'),
                    )), #end of lens info table

                html.H6('Manufacturer Engraving Template:', style={'textAlign':'center'}),
                html.Img(src='/assets/underlay2.png', className="underlay"),
                html.Div(style={
                    'clear':'both',
                    'padding-top' : '50px',
                    'padding-bottom' : '50px'
                }),

 
            ],className="app-body"),
          ] 
    )
])

# + 
# save_file(): Decodes and stores a file uploaded to the app, into the upload directory. 
#
# Parameters:
#   saveFileNameAs - [string] The filename to save the file as. 
#   fileContent    - [string] An encoded string containing the content of the file to save
# 
# Returns:
#   fileSavedPath  - [string] The path to the file that was saved
# - 
def save_file(saveFileNameAs, fileContent):
    # Decode the content of the file passed in as 'fileContent' 
    data = fileContent.encode("utf8").split(b";base64,")[1]

    # Create a new file called 'saveFileNameAs' OR overwrite that file if it already exists, and 
    # write to it the decoded file 'data' and save to the upload directory. 
    with open(os.path.join(UPLOAD_DIRECTORY, saveFileNameAs), "wb") as fp:
        fp.write(base64.decodebytes(data))

    # Return the path to the saved file
    fileSavedPath = os.path.join(UPLOAD_DIRECTORY, saveFileNameAs)
    return(fileSavedPath)

def parse_contents(contents, filename):

    return html.Div([
        html.Img(src=contents),
    ], id=filename)

# Callback for updating an uploaded image + the table results
@app.callback(
    Output('image-upload-left', 'children'), 
    Output('image-upload-right', 'children'),
    Output('table', 'data'),
    Output('editTable', 'data'),
    # Output('manufacturer', 'children'),
    Input('lens-images-left', 'contents'),
    Input('lens-images-right', 'contents'),
    Input('table-editing-input', 'data'),
    Input('table-editing-input', 'columns'),
    State('lens-images-left', 'filename'),
    State('lens-images-right', 'filename')
    )

def update_output(list_of_contents_left, list_of_contents_right, rows, columns, list_of_names_left, list_of_names_right):
    # Check if an image was uploaded to both the left and right upload boxes
    if list_of_contents_left is not None and list_of_contents_right is not None:
        # Parse the contents and filenames of files uploaded to the LEFT image upload box, and save to children_left
        children_left = [
            parse_contents(c, n) for c, n in zip(list_of_contents_left, list_of_names_left)
        ]

        # Parse the contents and filenames of files uploaded to the RIGHT image upload box, and save to children_right
        children_right = [
            parse_contents(c, n) for c, n in zip(list_of_contents_right, list_of_names_right)
        ]

        # Try to perform OCR on two input images - this requires:
        # 1. Two images must be uploaded (one in both the left and right lens side upload boxes)
        # 2. The image in the left lens-side upload is actually an image of the left lens side 
        #    (wont work if it's the right side) and vice versa
        try:
            # Save the uploaded files to the upload directory (saving locally this way simplifies things).
            # A simple naming system is used for the uploaded files (L and R .png) - old files with the 
            # same name will (intentionally) be overwritten. 
            leftPath  = save_file("L.png", list_of_contents_left[0])
            rightPath = save_file("R.png", list_of_contents_right[0])

            # Call the OCR and lens processing script to read the text of the lens
            OCRLeftDataFrame  = OCR.readTextFromImage(leftPath) 
            OCRRightDataFrame = OCR.readTextFromImage(rightPath) 
            #print(OCRLeftDataFrame, '\n', OCRRightDataFrame, '\n') # Print all detected text from the response for left and right sides of the lens
            
            # Read the manufacturer symbol using Optical Symbol Recognition (OSR) module
            OSRLeftLensResponse  = OSR.readSymbol(leftPath)

            OSRRightLensResponse = OSR.readSymbol(rightPath)
           
            # Decode the detected codes, call autocorrect scripts, and update OCRDescription 
            # Return the lens properties and the OCR Description to display to the user
            lensPropertiesDict, OCRDescription = decoder.decodeLens(OCRLeftDataFrame, OCRRightDataFrame, OSRLeftLensResponse, OSRRightLensResponse, False)
 
            # Call function to display detected symbol
            manufacturerPath = manufacturerImg(lensPropertiesDict)


            # Initialize Detected Text Dictionary
            lensDectectedTextDict = {
                "Left Symbol" : OSRLeftLensResponse[0]['classLabel'],
                "Left Text" : OCRDescription["OCRDescriptionLeft"],#",#OCRLeftDataFrame,
                "Right Symbol" : OSRRightLensResponse[0]['classLabel'],
                "Right Text" : OCRDescription["OCRDescriptionRight"]#OCRRightDataFrame
            }

        except Exception as e:
            # If an exception occurs, don't stop the program but print out a warning to the user to console, and 
            # call the Main function with "None" values to get back an empty lens properties dict. 
            print("EXCEPTION: Encountered the following exception: ", e)
            lensPropertiesDict, OCRDescription = decoder.decodeLens(None, None, None, None, None)
            lensDectectedTextDict = {
                "Left Symbol" : "N/A", 
                "Left Text" : "N/A",
                "Right Symbol" : "N/A",
                "Right Text" : "N/A"
            }

        # Place the resulting lens properties in a Pandas data frame, convert it to a dictionary (in a different 
        # format as the starting dictionary) and return both the left and right image data as well as the lens properties
        lensProperties = pd.DataFrame(lensPropertiesDict.items(), columns=['Title', 'Info']) 
        data = lensProperties.to_dict('records')

        detectedText = pd.DataFrame(lensDectectedTextDict.items(), columns=['Description', 'Detected Text'])
        text = detectedText.to_dict('records')


       
        #Check for User Inputs
        if all(ele['Detected Text'] == 'Enter user edits' for ele in rows):
            print("No User Input")
        else:
            #If user inputs call userUpdatedCodeDecode
            data, text = userUpdatedCodeDecode(rows, lensDectectedTextDict, data, text)
   
        return children_left, children_right, data, text

    # If there wasn't both a left and right image uploaded, there is no image data to display for either lens (return None)   
    else:
        return None, None, None, None


def manufacturerImg(propertiesDict):
        if(propertiesDict['Manufacturer'] == "Carl Zeiss"):
            return html.Div([
                html.Img(src="/Users/zahraabid/Documents/progressivelensid/assets/Manufacturers/Zeiss.png"),
            ])

        if(propertiesDict['Manufacturer'] == "Essilor"):
            return html.Div([
                html.Img(src="/Users/zahraabid/Documents/progressivelensid/assets/Manufacturers/Essilor.png"),
            ])

        if(propertiesDict['Manufacturer'] == "Nikon"):
            return html.Div([
                html.Img(src="/Users/zahraabid/Documents/progressivelensid/assets/Manufacturers/Nikon.png"),
            ])
        return propertiesDict['Manufacturer']
    
#Update Output data based on user inputed text
def userUpdatedCodeDecode(rows, orginalText, data, text):
    
    #copy user inputs that have been modified or if no modification copy auto detected text
    #Left Symbol
    if rows[0]['Detected Text'] != 'Enter user edits':
        OSRLeftUserInput = rows[0]['Detected Text'] #User Input
    else:
        OSRLeftUserInput = orginalText["Left Symbol"] #Orginal detected text
    
    #Right Symbol
    if rows[2]['Detected Text'] != 'Enter user edits':
        OSRRightUserInput = rows[2]['Detected Text'] #User Input 
    else:
        OSRRightUserInput = orginalText["Right Symbol"]#Orginal detected text
    
    #Left Text
    if rows[1]['Detected Text'] != 'Enter user edits': #User Input
        OCRLeftUserInput = rows[1]['Detected Text'].split((',')) #seperate words seperated by commas into unique indices
        for i in range(len(OCRLeftUserInput)):
            OCRLeftUserInput[i] = OCRLeftUserInput[i].strip() #remove spaces
    else:
        OCRLeftUserInput = orginalText["Left Text"]#Orginal detected text
    
     #Right Text
    if rows[3]['Detected Text'] != 'Enter user edits': #User Input
        OCRRightUserInput = rows[3]['Detected Text'].split(',') #seperate words seperated by commas into unique indices
        for i in range(len(OCRRightUserInput)):
            OCRRightUserInput[i] = OCRRightUserInput[i].strip() #remove spaces
    else:
        OCRRightUserInput = orginalText["Right Text"] #Orginal detected text

    #Translate user input to OCR dataframe format
    #Assumes user input is in the same from of the OCRDescription arrays (ex. ['275', '404'])
    OCRLeftDataFrame = decoder.translateUserToOCRDataFrame(OCRLeftUserInput)
    OCRRightDataFrame = decoder.translateUserToOCRDataFrame(OCRRightUserInput)
    
    #Translate OSR input to array of single element to match formatting
    OSRLeftLensResponse=[]
    OSRLeftLensResponse.append(OSRLeftUserInput)
    OSRRightLensResponse=[]
    OSRRightLensResponse.append(OSRRightUserInput)

    #Call decoder function to map inputs to results
    lensPropertiesDict, OCRDescription = decoder.decodeLens(OCRLeftDataFrame, OCRRightDataFrame, OSRLeftLensResponse, OSRRightLensResponse, True) 

    
    # Place the resulting lens properties in a Pandas data frame, convert it to a dictionary (in a different 
    # format as the starting dictionary) and return both the left and right image data as well as the lens properties
    lensProperties = pd.DataFrame(lensPropertiesDict.items(), columns=['Title', 'Info']) 
    data = lensProperties.to_dict('records')
    
    # Initialize Detected Text Dictionary
    lensDectectedTextDict = {
        "Left Symbol" : OSRLeftLensResponse,
        "Left Text" : OCRDescription["OCRDescriptionLeft"],
        "Right Symbol" : OSRRightLensResponse,
        "Right Text" : OCRDescription["OCRDescriptionRight"]
    }
    
    #print(lensDectectedTextDict)
    detectedText = pd.DataFrame(lensDectectedTextDict.items(), columns=['Description', 'Detected Text'])
    text = detectedText.to_dict('records')

    return data, text


#Function to allow testing without using app
#comment app.run_server(debug=True) in __main__ and call test() to use
def test():
    leftPath  = "images/WIN_20210217_13_24_38_Pro.jpg"
    rightPath = "images/WIN_20210217_13_24_50_Pro.jpg"

    # Call the OCR and lens processing script
    OCRLeftDataFrame = OCR.readTextFromImage(leftPath) 
    OCRRightDataFrame = OCR.readTextFromImage(rightPath) 
    print(OCRLeftDataFrame, '\n', OCRRightDataFrame, '\n') # Print all detected text from the response for left and right sides of the lens

    #Read the manufacturer symbol using Optical Symbol Recognition (OSR) module
    OSRLeftLensResponse  = OSR.readSymbol(leftPath)
    OSRRightLensResponse = OSR.readSymbol(rightPath)
    
    #Decode the detected codes, call autocorrect scripts, and update OCRDescription 
    #Return the lens properites and the OCR Description to display to the user
    lensPropertiesDict, OCRDescription = decoder.decodeLens(OCRLeftDataFrame, OCRRightDataFrame, OSRLeftLensResponse, OSRRightLensResponse, False) 

    print(OCRDescription["OCRDescriptionLeft"])
    print(OCRDescription["OCRDescriptionRight"])
    



if __name__ == '__main__':
    app.run_server(debug=True)
    # test()
 