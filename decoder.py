# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 14:29:48 2020

@author: Geoff, Vicky
# """
import Extraction
import json
import pandas as pd

def decodeLens(OCRLeftDataFrame, OCRRightDataFrame, OSRLeftLensResponse, OSRRightLensResponse, userInput):
    # Initialize OCR description to output to user to validate results
    # Must be updated to show changes made to results via autocorrection features
    OCRDescriptionLeft = []
    OCRDescriptionRight = []

    # Initialize dictionary of code paramters
    codes = dict.fromkeys(['ADD', 
                        'frame_fit',
                        'vision_profile',
                        'base_curve',
                        'MID',
                        'product',
                        'material',
                        'fit_height'])
    
    # Extract info from the codes detected
    isTemporal_left  = Extraction.lenInformationExtraction(OCRLeftDataFrame, codes, OCRDescriptionLeft)
    isTemporal_right = Extraction.lenInformationExtraction(OCRRightDataFrame, codes, OCRDescriptionRight)
    
    OCRDescription = {
        "OCRDescriptionLeft"  : OCRDescriptionLeft,
        "OCRDescriptionRight" : OCRDescriptionRight
    }
    # Iterate over items in dict and print line by line for testing
    [print(key + ':', value) for key, value in codes.items()]
    print('\n')

    #Check for no OSR Response
    if(not OSRLeftLensResponse or not OSRRightLensResponse):
        lensProperties = returnEmptyResponse()
    else:
        try:
            if userInput:
                labelsLeftLens = OSRLeftLensResponse
                labelsRightLens = OSRRightLensResponse
            else:
                # Decode the manufacturer symbol using Optical Symbol Recognition (OSR)response
                labelsLeftLens   = [objDict['classLabel'] for objDict in OSRLeftLensResponse] # TODO: Should the two manufacturer labels match? 
                labelsRightLens = [objDict['classLabel'] for objDict in OSRRightLensResponse]
            # Decode the product, material, etc. codes from alphanumerical and labels, to actual strings to be 
            # displayed on the UI, ex. "L5" product code --> "Super Max Vision Plus"
            manufacturerDetected = decodeManufacturer(labelsLeftLens, labelsRightLens)
            materialDecoded = decodeMaterialType(codes["material"], manufacturerDetected)
            productLineDecoded = decodeProduct(codes["product"], manufacturerDetected)
            addDecoded = decodeADD(codes["ADD"], manufacturerDetected)
            frameFitDecoded = decodeFrameFit(codes["frame_fit"], manufacturerDetected)
            visionProfileDecoded = decodeVisionProfile(codes["vision_profile"], manufacturerDetected)
            midDecoded = decodeMID(codes["MID"], manufacturerDetected)
            fitHeightDecoded   = decodeFitHeight(codes["fit_height"], manufacturerDetected)
            glassesSideDecoded = decodeGlassesSide(isTemporal_left, isTemporal_right, manufacturerDetected)
            
            # Construct the dictionary with the detected and decoded lens properties
            lensProperties = {
                "Manufacturer" : manufacturerDetected,
                "Product Line" : productLineDecoded,
                "Material" : materialDecoded,
                "ADD" : addDecoded,
                "Frame Fit" : frameFitDecoded,
                "Base Curve" : codes["base_curve"],
                "Vision Profile" : visionProfileDecoded,
                "MID" : midDecoded,
                "Fit Height" : fitHeightDecoded,
                "Glasses Side" : glassesSideDecoded
            }
            
            # Iterate over items in dict and print line by line for testing
            [print(key + ':', value) for key, value in lensProperties.items()]
            print('\n')

        except Exception as e:
            print("EXCEPTION in decoder.py: ", e)
            OCRDescription = returnEmptyResponse()
   
    return lensProperties, OCRDescription

def returnEmptyResponse():
    # Construct the dictionary with the detected and decoded lens properties
    lensProperties = {
        "Manufacturer" : "N/A", 
        "Product Line" : "N/A",
        "Material" : "N/A",
        "ADD" : "N/A",
        "Frame Fit" : "N/A",
        "Base Curve" : "N/A",
        "Vision Profile" : "N/A",
        "MID" : "N/A",
        "Fit Height" : "N/A",
        "Glasses Side" : "N/A"
    }

    return lensProperties

def translateUserToOCRDataFrame(userInputArray):
     df = pd.DataFrame(columns=['description'])
     
     #loop through lines of user input and save to data frame to match the OCR format
     for i in range(len(userInputArray)):
         word_text = userInputArray[i]
         df = df.append(dict(description=word_text), ignore_index=True)
    
     return df
 
def translateUserToOSRResponse(userInput):
      return 

#############################################################
#Decode Functions 
#############################################################
def decodeMaterialType(codedMaterial, brandName):
    materialDecoded = None
    default = None # The default material for the brand, decoded. May or may not be defined in the JSON.  

    try:
        material = float(codedMaterial)
    except:
        material = codedMaterial
    
    with open('lensLookupData.json') as jsonFile:
        lensData = json.load(jsonFile)
    
    dictionary = lensData[brandName]["materials"] 
    for item in dictionary:
        for key in item.keys():
            if(key == str(material)):
                materialDecoded =  item[key]
                break
            elif(key == ""):
                # A default is symbolized by a "" in the JSON. If a default value is defined, store it. 
                default = item[key]

    # If the material was not found in the JSON, but a default exists, let the material be the default value. 
    if ((materialDecoded is None) and (default is not None)):
        materialDecoded = default

    return(materialDecoded)
    
def decodeProduct(codedProduct, brandName):
    productLineDecoded = None
    with open('lensLookupData.json') as jsonFile:
        lensData = json.load(jsonFile)
        
    dictionary = lensData[brandName]["product"]
    for item in dictionary:
        for key in item.keys():
            if(key == str(codedProduct)):
                productLineDecoded = item[key]
    print(productLineDecoded)

    return (productLineDecoded)

def decodeADD(codedADD, brandName):
    addDecoded = None
    try:
        addDecoded = float(codedADD)/100
    except:
        addDecoded = codedADD
        
    return (addDecoded)

             
def decodeFrameFit(codedFrameFit, brandName):
    frameFitDecoded = None
    try:
        frameFit = float(codedFrameFit)/10
    except:
        frameFit = codedFrameFit
            
    with open('lensLookupData.json') as jsonFile:
        lensData = json.load(jsonFile)
    
    dictionary = lensData[brandName]["frameFit"]
    for item in dictionary:
        for key in item.keys():
            if(key == str(frameFit)):
                frameFitDecoded = item[key]
                break
    return (frameFitDecoded)
               
def decodeVisionProfile(codedVision, brandName):
    visionProfileDecoded = None
    with open('lensLookupData.json') as jsonFile:
        lensData = json.load(jsonFile)
    
    dictionary = lensData[brandName]["visionProfile"]
    for item in dictionary:
        for key in item.keys():
            if(key == str(codedVision)):
                visionProfileDecoded = item[key]
                break
    return (visionProfileDecoded)

def decodeMID(codedMID, brandName):
    midDecoded = None
    
    if (codedMID != None):
        midDecoded = codedMID + 'cm'
        
    return (midDecoded)

def decodeFitHeight(codedHeight, brandName):
    fitHeightDecoded = None
    try:
        Height = float(codedHeight)
    except:
        Height = codedHeight
        
    with open('lensLookupData.json') as jsonFile:
        lensData = json.load(jsonFile)

    dictionary = lensData[brandName]["fitHeight"]
    for item in dictionary:
        for key in item.keys():
            if(key == str(Height)):
                fitHeightDecoded = item[key]
                break
    return (fitHeightDecoded)

def decodeGlassesSide(isTemporal_left, isTemporal_right, brandName):
    glassesSideDecoded = 'Unknown'

    if (isTemporal_left==True and isTemporal_right==False):
        glassesSideDecoded ='Left'
    
    elif (isTemporal_right==True and isTemporal_left==False):
        glassesSideDecoded = 'Right'
        
    return (glassesSideDecoded)

def decodeManufacturer(leftLabels, rightLabels):
    if (leftLabels and rightLabels):
        # In general, each label array is a list of size = 1. If size > 1 just use the first element. 
        # TODO: Right now, we only use the first item in the array b/c we assume the only logo is manufacturer logo.
        # Future updates will have more than one logo, in which case additional logic will need to be added. 
        leftLabel  = leftLabels[0]
        rightLabel = rightLabels[0]

        # These two labels should match - if not, throw an error because we don't know which manufacturer's data to use, 
        if (leftLabel == rightLabel):
            with open('lensLookupData.json') as jsonFile:
                lensData = json.load(jsonFile)

            # Find the NN_Label matching leftLabel and rightLabel in the JSON
            for brand in lensData.keys():
                if (leftLabel in lensData[brand]['brandIdentifier']):
                    return brand

            # If we didn't return already, that means the manufacturer's data is not in the JSON - return None
            return None
        else:
            return None
    else:
        return None
    
