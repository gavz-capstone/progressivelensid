# +
# @author: Vicky
# This script attempts to find the best paramters for preprocessing (in particular, the Gaussian Adaptive Threshold
# method) based off of accuracy scores of the OCR on a known image. 
# -

# +
# Library imports
# -
import cv2 as cv
import os, io
from google.cloud import vision
import pandas as pd

# +
# Initialize Google OCR interaction
# -
confidence_thresh=0.8

# Set the environment variable of GOOGLE_APPLICATION_CREDENTIALS to the path to our JSON token file. 
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.abspath('ServiceAccountToken.json')

# Initialize the client environment for text recognition
client = vision.ImageAnnotatorClient()

# + 
# OCR(): Takes an input preprocessed image and calls Google's text
# recognition API. The part of the response relating to the text detected is returned to the user in a DataFrame 
# object.
#
# Parameters: 
#   input_image  - [cv2 image] The preprocessed image to read text from 
#
# Returns:
#   df           - [DataFrame object] Text recognition part of the response returned from Google's text recongition API
#
# -
def OCR(input_image):
    # Convert the input image to a byte stream, and save it to "content"
    success, encoded_image = cv.imencode('.png', input_image)
    content = encoded_image.tobytes()

    # Run text detection on the preprocessed image using the Google API
    image    = vision.Image(content=content)
    response = client.document_text_detection(image=image)
    
    # Annotate Image Response, and save to DataFrame, "df"
    df = pd.DataFrame(columns=['description', 'confidence'])

    # Extract the relevant parts of the returned response
    for page in response.full_text_annotation.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                for word in paragraph.words:
                    word_text = ''.join([
                        symbol.text for symbol in word.symbols
                    ])
                    
                    # Save word from each line to data frame - Only save those with a high confidence
                    if (word.confidence>=confidence_thresh):
                        df = df.append(
                            dict(
                                description=word_text,
                                confidence=word.confidence
                            ), ignore_index=True
                        )

    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))

    return df

# +
# Experiment with different block sizes
# -
filename = f"images\lensL.jpg" # The test mage to be used
knownResponseL1 = "275"  # The first line of text in the image should read '275'
knownResponseL2 = "404"  # The second line of text in the image should read '404'

img = cv.imread(filename,0)    
correctDetected = []    # A list of the parameters that guessed the image text correctly

# Iterate over all odd numbers from 3 to 99 for blocksize and 1 - 20 for 'c' param
for blocksize in range(3, 101, 2):
    for c in range(1,21): 
        # Apply image preprocessing, and perfrom OCR on the image - the result is stored in DataFrame object, df
        th2 = cv.adaptiveThreshold(img,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,cv.THRESH_BINARY,blocksize,c)
        df = OCR(th2)

        # If the dataframe returned from OCR has data in it (not None or empty), check if it guessed the
        # text in the image correctly and at what confidence level. 
        if(df is not None and not df.empty):
            try:
                line1 = df['description'][0]
                line2 = df['description'][1] 
                confidence1 = df['confidence'][0]
                confidence2 = df['confidence'][1] 
                
                # Compare Response - if this is True, it guessed correctly, so add the relevant values
                # as a dictionary into the correctDetected list
                if(line1 == knownResponseL1 and line2 == knownResponseL2):
                    correctDetected.append({
                        "line1" : df['description'][0],
                        "line2" : df['description'][1] ,
                        "confidence1" : df['confidence'][0],
                        "confidence2" : df['confidence'][1],
                        "blocksize"   : blocksize,
                        "c" : c 
                    })

            except:
                continue
            

# Rank confidence totals:
confHigh = []
for conf in correctDetected:
    sum = conf['confidence1'] + conf['confidence2']
    conf["confidenceSum"] = sum

# Print the sorted list of confidences, with the highest sum of confidences at the end of the list (best), lowest
# sum (worst) at the start. 
print(sorted(correctDetected, key = lambda i: i['confidenceSum']))


