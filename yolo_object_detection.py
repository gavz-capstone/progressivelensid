# +
# @author: Vicky
# Given an input image, apply Optical Symbol Recognition (OSR) using a trained neural network to identify 
# manufacturer lens logos in the image (ex. Carl Zeiss, Essilor, etc.)
#
# Key Dependencies: These hold the information from the training process of the NN
#   yolov3_testing.cfg
#   yolov3_training_last.weights
# -

# +
# Library imports
# -
import cv2
import numpy as np
import glob
import random
import os
import sys

# Load Yolo and trainined data
net = cv2.dnn.readNet("yolov3_training_last.weights", "yolov3_testing.cfg")

# Logo classes to detect (includes all classes the net is trained on thus far)
classes = ["Zeiss_Z"]

#+
# readSymbol(): This method takes an input image and passes it to the NN to perform symbol recongition. Outputs all
#               symbols the NN recognized in the image. 
#
# Parameters:
#   imagePath - [string] The filepath (including file name) of the image to perform OSR on 
# 
# Returns:
#   labels    - [List] The classes detected in the image using OSR (multiple labels being detected is possible, hence a list)
# - 
def readSymbol(imagePath):
    layer_names = net.getLayerNames()
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    # Load the input image
    img = cv2.imread(imagePath)
    img = cv2.resize(img, None, fx=0.4, fy=0.4)
    height, width, channels = img.shape

    # Detect objects
    blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 0, 0), True, crop=False)
    net.setInput(blob)
    outs = net.forward(output_layers)

    # Initialize variables to store information if the detected symbols - class id, location and confidence of detection
    class_ids   = []
    confidences = []
    boxes = []

    # Get the confidence scores for detection, location of detected symbol, and class information for each detected symbol.  
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]

            # If the confidence of detection is greater than 0.3 (i.e. 30%), assume the object was correctly detected
            if confidence > 0.3:
                # Object detected - get the 4 co-ordinates that describe the corners of the detection box
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                # Append the detected object's information to the correct lists so we can access them later
                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
        
    # 'response' is a list that will store the response to be returned to the caller. Each item in the list is a
    # dictionary object, containing information on each detected symbol in the image. 
    response = [] 

    # Iterate over all detected objects, and format a response to return to the caller
    for i in range(len(boxes)):
        if i in indexes:
            # Build an object to describe the current detected item, and add to the response list
            response.append({
                'boxCoords'  : boxes[i],                    # The corner co-ordinates of the detection box, Ex. [283, 111, 75, 73]
                'classId'    : class_ids[i],                # The numerical class id, Ex. 0
                'classLabel' : str(classes[class_ids[i]]),  # The string name for the class Id, ex. Carl Zeiss
                'confidence' : confidences[0]               # The confidence of symbol recognition, Ex. 0.998
            })

    return response
