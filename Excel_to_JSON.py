# +
# @author: Vicky
# Scrape an Excel file containing various lens info (i.e. material, design data, etc.) for multiple progressive lens manufacturers, and output
# the data into manufacturer-specific lookup dictionaries in a JSON
#
# Dependencies:
#   xlrd library for reading excel sheets: pip install xlrd
# -

# +
# Library imports
# -
import json
import xlrd
import os

# Create a new dictionary (that will later be converted into a JSON)
data = {}

# Open the Excel file so that we can read its contents
fileName = "data/UniversalLUT.xlsx"
xl_workbook = xlrd.open_workbook(filename=os.path.abspath(fileName))


# +
# Parse engravings sheet and get engraving info/symbols/notes, etc. 
# -
# Retrieve the "engravings" sheet from the workbook
engravingSheet = xl_workbook.sheet_by_name('Engravings') 

# The first row in the sheet is column names - strip leading and trailing whitespace from the name
cellColumnNames = [(cell.value).strip() for cell in engravingSheet.row(0)] 

# Get the position of the columns of interest - these might change, so its best to dynamically get their index
symbolCodeIndex = cellColumnNames.index("Engraving Alphanumeric")
NNLabelIndex    = cellColumnNames.index("NN Label")
brandIndex      = cellColumnNames.index("Brand")
referenceIndex  = cellColumnNames.index("Reference Mark")
productIndex    = cellColumnNames.index("Product")
materialIndex   = cellColumnNames.index("Material")
frameFitIndex   = cellColumnNames.index("FrameFit Value")
visionProfIndex = cellColumnNames.index("Vision Profile")
fitHeightIndex  = cellColumnNames.index("Fit Height Design")
brandIDIndex    = cellColumnNames.index("Brand Identifier")

# The following columns are not currently tracked/added to the JSON: 
#notesIndex      = cellColumnNames.index("Notes")
#lensSideIndex   = cellColumnNames.index("Lens Side")
#discontIndex    = cellColumnNames.index("Discontinued?")
#dependencyIndex = cellColumnNames.index("Dependency")

# Iterate through every row of the Engravings sheet, and extract the data. Append the extracted data into the "data" object as dictionary objects
for rowIndex in range(1, engravingSheet.nrows): # Start iteration at row 1 (row 0 is just the column titles)
    # First get the brand name (i.e. manufacturer name)- do they exist already in the current "data" object?
    brandName = engravingSheet.cell_value(rowIndex, brandIndex)

    if (brandName not in data.keys()): 
        # Brand name data currently does not yet exist in the JSON. Initialize all entries for this brand.
        data[brandName] = {}
        data[brandName]["product"]   = [] # productIndex
        data[brandName]["materials"] = [] # materialIndex
        data[brandName]["frameFit"]  = [] # frameFitIndex
        data[brandName]["visionProfile"] = [] # visionProfIndex
        data[brandName]["fitHeight"] = [] # fitHeightIndex
        data[brandName]["referenceMark"]    = [] # NNLabelIndex and referenceIndex
        data[brandName]["brandIdentifier"]  = [] # brandIDIndex

    # Find which data the current row describes (i.e. product, materials, framefit, etc.). There should only be ONE of these in this row (else, ignore)
    # Get all of the aforementioned cell data for the current row, and place in a dictionary for easier parsing:
    rowData = {
        'referenceMark'   : engravingSheet.cell_value(rowIndex, referenceIndex),
        'product'         : engravingSheet.cell_value(rowIndex, productIndex),
        'materials'     : engravingSheet.cell_value(rowIndex, materialIndex),
        'frameFit'      : engravingSheet.cell_value(rowIndex, frameFitIndex),
        'visionProfile' : engravingSheet.cell_value(rowIndex, visionProfIndex),
        'fitHeight'     : engravingSheet.cell_value(rowIndex, fitHeightIndex)
    }
    
    # Check which of these cells is NOT empty (if all empty, continue to next for loop iteration)
    key, val = next(((k, v) for k, v in rowData.items() if v), (None, None))
    if (not val):
        continue 
    else:
        # Get the value of the "code" cell (this will be the key to the dictionary entry)
        codeCellValue = engravingSheet.cell_value(rowIndex, symbolCodeIndex)

        # Validate the "code" value - it can be alphanumeric or a special character (i.e. a number or string of any kind, even empty string)
        if (isinstance(codeCellValue, (str, int, float)) and (codeCellValue is not None)):
            codeCellValue = str(codeCellValue)

            # If the "code" cell is empty, pull the label from the NN Label column instead (indicates code is a symbol/logo/image, not text)
            if (codeCellValue == ""):
                NNLabelCellValue = engravingSheet.cell_value(rowIndex, NNLabelIndex)
                codeCellValue = NNLabelCellValue # Let the code in this case be the NN Label

                # If this is additionally used as a brand identifier, add the NN label to the brand identifier list
                # Ensure we aren't using empty "" NN labels to avoid duplication issues in the list (just ignore empty ones)
                isBrandIdentifier = engravingSheet.cell_value(rowIndex, brandIDIndex)
                if(isBrandIdentifier == "Y" and codeCellValue != ""):
                    data[brandName]["brandIdentifier"].append(codeCellValue)

                # If this is the manufacturer symbol NN label (reference mark column == "Y") build a special list (NOT a dictionary)
                # This represents the possible NN labels for different symbols that all describe the same brand name
                if(val == "Y"):
                    data[brandName]['referenceMark'].append(codeCellValue)
                    continue # Continue to the next iteration of the for loop (avoids the dictionary append as we only want a list here)

        else:
            codeCellValue = "Code Unavailable"

        # Add the data to the corresponding JSON object dictionary for the manufacturer specified by brandName
        newMaterial = {codeCellValue : val}
        data[brandName][key].append(newMaterial)

# +
# Write formatted data out to JSON file called 'lensLookupData.json'
# -
with open('lensLookupData.json', 'w') as outfile:
    json.dump(data, outfile, indent=4)