# +
# @author: Vicky
# This script contains methods to run the preprocessing methods on a single file, or all files within a specified folder. 
# Currently, Gaussian Adaptive Thresholding is used. The result is either returned to the user, or saved to the same folder 
# as the input file, with filename the same as the input file and "_Processed.jpg" appended to the end. 
#
# Note: Processed images are saved as .jpg. See method descriptions for more details. 
#
# -

# +
# Library imports
# -
import cv2 as cv
import glob
import os

# +
# preprocessImage(): This method preprocesses a single input image, and returns it to the user.
#
# Parameters:
#   inputFilename - [string] The filepath and/or name of the image to process (ex. img1.jpg, or images/img1.png), 
#                            relative to the directory of this script. 
# 
# Returns:
#   th2           - [binary image] Return the preprocessed image to the user
# - 
def preprocessImage(inputFilename):
    # Try to read image in and apply adaptive thresholding to it with blocksize = 99, and C = 13. 
    # Raise an exception if this fails - the image may not have been able to be located
    try:
        img = cv.imread(inputFilename,0)
        th2 = cv.adaptiveThreshold(img,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,cv.THRESH_BINARY,99,13)
    except:
        print("ERROR: Could not read/process image: ", inputFilename)
        th2 = None
         
    # Return the preprocessed image
    return th2

# +
# preprocessImageAndSave(): This method preprocesses a single input image, and saves it to the same folder
# as the input image. 
#
# Parameters:
#   inputFilename  - [string] The filepath and/or name of the image to process (ex. img1.jpg, or images/img1.png), 
#                             relative to the directory of this script. 
# 
# Returns:
#   fileNameToSave - [string] The filename of the preprocessed image that was saved to the folder (ex. "img1_Processed.jpg")
# - 
def preprocessImageAndSave(inputFilename):
    # Call the method to preprocess the image - this returns a binary image, which will later be saved
    th2 = preprocessImage(inputFilename)

    # Get the filename and directory to save the processed image to.
    saveDirName = os.path.dirname(os.path.abspath(__file__)) 
    fileNameToSave = os.path.splitext(inputFilename)[0] + "_Processed.jpg"

    # Save the preprocessed image "th2" to the absolute  path specified by "saveDirName", with the file name "fileNameToSave"
    cv.imwrite(os.path.join(saveDirName , fileNameToSave), th2)

    return fileNameToSave # Return the output file name that the processed image was saved as. 

# +
# preprocessFolderAndSave(): This method preprocesses all files in a specified folder, and saves the processed
# images to the folder.
#
# Parameters:
#   folderName - [string] The name of the folder with the images to process
# 
# Returns:
#   N/A
# - 
def preprocessFolderAndSave(folderName):
    # Process all .jpg and .png images
    filesInFolder = glob.glob(folderName + '/*.jpg')  + glob.glob(folderName + '/*.png')

    for filename in filesInFolder:
        # If image is a "Processed" image, skip this image and continue to the next (because this is image already has 
        # the thresholding applied to it). 
        if ("_Processed.jpg" in filename):
            continue

        # Process the image, and save it to the current folder - right now, we don't do anything with the output filename
        outputFilename = preprocessImageAndSave(filename)
        
# +
# preprocessMultipleFolders(): This methood preprocesses all images in a list of folders, and saves the processed
# images to the corrresponding folder. 
#
# Parameters:
#   folderNameList - [List of strings] A list of the folder names containing images to preprocess. Ex: folderNameList = ["Z", "images"]
# 
# Returns:
#   N/A
# - 
def preprocessMultipleFolders(folderNameList):
    # Iteration loop - Iterate over all specified folders, apply the adaptive thresholding filter to each image in the folder,
    # and save the new processed image to the corresponding folder. 
    for folder in folderNameList:
        preprocessFolderAndSave(folder)
